import logo from "./logo.svg";
import "./App.css";
import FunctionComponent from "./DemoComponent/FunctionComponent";
import ClassComponent from "./DemoComponent/ClassComponent";
import Ex_Layout from "./Ex_Layout/Ex_Layout";

function App() {
  return (
    <div className="App">
      {/* <FunctionComponent /> */}
      {/* <ClassComponent /> */}

      <Ex_Layout />
    </div>
  );
}

export default App;
